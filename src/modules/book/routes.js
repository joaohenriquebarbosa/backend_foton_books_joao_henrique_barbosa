const router = require("express").Router();
const booksController = require("./controller");

router
  .route("/")
  .get(booksController.getBooks)
  .post(booksController.createBook);

router
  .route("/:bookId")
  .get(booksController.getBook)
  .put(booksController.updateBook)
  .delete(booksController.deleteBook);

module.exports = router;
